importScripts('/os/app_api.js?d=' + Math.floor(Date.now() / 1000) + ''); 
startup();

function startup(){
	app_api({'command': 'app_settings', 'name': 'width'},'800');
	app_api({'command': 'app_settings', 'name': 'height'},'500');
	app_api({'command': 'app_settings', 'name': 'name'},'Enchant Browser');
	app_api({'command': 'app_settings', 'name': 'icon_visable'},'true');
	app_api({'command': 'app_settings', 'name': 'icon_url'},'/icon.png');

	app_api({'command': 'luna_render_css', 'tag': '.menu'},'height:30px;padding:5px;border-bottom:1px solid rgb(228,230,234);');
app_api({'command': 'luna_render_css', 'tag': '.bookmark'},'display:inline-block;background:rgba(255,255,255,0.1);border:1px solid rgb(228,230,234);margin-left:3px;margin-right:3px;padding:3px;margin-top:2px;padding-left:5px;padding-right:5px;border-radius:3px;color:#555555;text-decoration:none;');

	var windowcode="<div class='menu'>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://www.duckduckgo.com\");' class='bookmark'>DuckDuckGo</a>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://www.wikipedia.org\");' class='bookmark'>Wikipedia</a>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://www.twii.me\");' class='bookmark'>Twii</a>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://www.codebee.io\");' class='bookmark'>CodeBee</a>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://www.deviantart.com\");' class='bookmark'>Deviantart</a>";
	windowcode=windowcode+"<a href='javascript:app_pass(\"enchant\",\"launch\",\"http://spotify.com\");' class='bookmark'>Spotify</a>";
	windowcode=windowcode+"</div><div id='site_frame' style='height: -moz-calc(100% - 41px);height: -webkit-calc(100% - 41px);height: calc(100% - 41px);width:100%;overflow:hidden;'><iframe sandbox=\"allow-same-origin allow-scripts allow-popups allow-forms\" src=\"https://www.wikipedia.org\" style='width:100%;height:100%;border:0px;'></iframe></div>";
	app_api({'command': 'luna_render_window', 'title': 'Enchant Browser', 'type': 'window'},windowcode);
}

function launch(site){
	var window="<iframe sandbox=\"allow-same-origin allow-scripts allow-popups allow-forms\" src=\"" + site + "\" style='width:100%;height:100%;border:0px;'></iframe>";
	app_api({'command': 'luna_element_content', 'elm': 'site_frame'},window);
}
